Title: Overview
URL:
save_as: index.html
Category: Overview


<img class="resize" style="float:right" width="195" height="277" margin:auto" src="https://needsmore.xyz/profile_pic/headshot_slice_mirror.png"/>

Data is beautiful and there's so much to learn.

Hi, my name is Casper and I have a passion for data & programming and a love for mathematics. My language of choice is Python for almost all things
considered though I do not fear treading into murkier waters to achieve my goals. With a work hard, get stuff done attitude I like to throw myself into challenges to learn and 
evolve. Also I can type pretty fast. 

Currently experimenting with a self-hosted dynamic Hadoop cluster to learn more about using Dask at scale on the reddit comments data (>1TB pure json) [link](https://files.pushshift.io/reddit/comments/).


###**Professional** (only relevant)


#### Full-time Data Scientist / Consultant @ [Codebeez](https://codebeez.nl/) November 2019 - Current
As a consultant contractor for Codebeez I am helping other companies in their development of Data Science Python programs in short term projects with high stakes. As my first client I landed a Data Scientist role at [CoNet](https://conet.nl/) where I continue to lead the development of their data science solution for their clients. These solutions consist first and foremost of predicting sensory values based on historic bases, but also doing optimization work with heuristics and performing predictive maintenance with survival models. 

####Full-time Data Scientist / Data Engineer / Consultant @ [KPN ICT Consulting](https://www.kpn.com/zakelijk/ict-consulting.htm) May 2017 - November 2019 
In my previous job as Data Engineer / Scientist at KPN ICT Consulting I have developed and delivered multiple best practices for an end-to-end data science solution that have been adopted by the Data & Analytics team of consultants. Some examples are suggesting Apache Airflow for our scheduling tasks, suggesting Dask for scaling our Python analysis operations and writing our preprocessing code as scikit-learn Transformers to keep it clean. I’ve had the lead as data engineer and scientist for multiple projects where we’ve implemented end-to-end machine learning solutions, starting with intakes on what datasets are available, all the way to generating insights from the forecasts. Mostly in the domain of predictive maintenance and customer churn. Here I have leveraged multiple models ranging from Markov Chains, XGBoost survival model, Random Forest Classifiers and time series models.  Furthermore a new recent favorite library is SHAP in combination with similar techniques to gather insights from the machine learning models. While I am an enthusiast on the technical parts, I do understand the business and their needs and love to translate these into concrete solutions. Creating a business case around a data science business question is a daunting challenge but one I love to think about.
###### _Nominated Star of the Year 2018: Best Technician_

#### Data Scientist / Data Engineer  @ [Ymere](https://www.ymere.nl/) May 2019 - Current (Project for KPNIC)
Retrieve, connect and streamline multiple data sources in a datalake in order to connect with the reporting dashboards and machine learning pipeline. In addtion I support in the validation of the machine learning model and optimization algorithm selection and design. 

#### Data Scientist / Data Engineer @ [Dutch Government](https://www.rijksoverheid.nl/) Oktober 2017 - Current (Project for KPNIC)
Technical lead in the data engineering team. Responsible for delivering well prepared tables for the use of collegues for further insight and making sure that a pipeline is available to refresh these tables.
While not busy preparing tables and keeping the pipeline alive I'm supporting the data science team by setting up systems (Dask) to scale their operation and testing new models.

#### Data Scientist / Data Engineer @ [KPN](https://www.kpn.com/) February 2019 - May 2019 (Project for KPNIC & Graduation)
Combining several data sources with network information, weather information and event data to create a model that will provide an accurate forecast on what node with generate an alarm - and what the root cause of this alarm is.
Used XGBoost survival model in combination with Markov chains to predict relative hazard per node. [pdf link](https://needsmore.xyz/extra/Scriptie_Casper_Lubbers.pdf)

#### Data Scientist / Data Engineer @ [Consolidated](https://www.consolidated.nl/) Oktober 2018 - Februari 2019 (Project for KPNIC)
Implemented and improved the PoC in a production ready environment. With the use of Docker, Airflow and AWS EC2 instances I lead the deployment of the model in a production ready environment. In addition I improved the accuracy of the prediction model.

#### Data Scientist / Data Engineer @ [KPN Internedservices](https://www.internedservices.nl/) June 2017 - June 2018 (Project for KPNIC)
Worked in a team of 5 to deliver a production ready Datalake solution that links a large amount of diverse data sources to provide insights and forecasts on a daily basis.
Responsible for the design and implementation of the datalake, as well as the forecasts. Forecasted customer churn and customer NPS. Tools used were exclusively Python.

#### Lead Software engineer / Data Scientist @ [KPN ICT Consulting](https://www.kpnconsulting.nl/)  October 2017 - June 2018 (Project for KPNIC)
Pitched the [Timelord](https://gitlab.com/timelord/timelord) project and lead a team of 5 on a one-day-a-week basis. Provided guidance and design input for the team members and contributed most of the core and utilities. Also spend a considerable amount of time researching multivariate time series models.

#### Data Scientist / Data Engineer @ [Ymere](https://www.ymere.nl/) June 2017 - November 2017 (Project for KPNIC)
Advised on improving the forecast accuracy by refining the features and performing dimensionality reduction. The goal was to predict what specific roof will leak in a given month as proof of concept. This PoC will be expanded vastly on functionality and will be brought into production with my role as advising data engineer. 

#### Data Scientist / Data Engineer @ [Telfort](https://www.telfort.nl/) June 2017 - October 2017 (Project for KPNIC)
Improved the model design to improve accuracy of forecasting customer statisfaction after customer support calls. Delivered actionable insights by fitting an appropriate model and extracting valueable features from complicated log files. Expanded the already existing pipeline and implemented it production ready to run on an hourly basis.

####Full-time Data Engineer / Scientist @ [CashRocket / SporeBI](http://cashrocket.io/) March 2016 - May 2017
Responsible for the architecture of the ETL of hundreds of bookkeepings of SME's through the [Exact Online API](https://developers.exactonline.com/) for constant up-to-date insights. 
These insights consisted of numerous KPI's but most notably of several time series models in an ensemble configuration (combining RNNs and classical time series models) and clustering methods (ranging from K-means to Dynamic Time Warping). 
Completely build in Python and R utilizing libraries such as Forecast (R), Tensorflow (python) and Scikit-Learn (python).


####Part-time Teamleader @ [Albert Heijn](https://www.ah.nl/) 2010-2011
The go-to person in a team varying from 10-20 members. Responsible for everyone doing their job properly and general trouble-shooting.

###**Studies**


| Study                  | University                               | Date           |
|------------------------|------------------------------------------|----------------|
| Applied Mathematics    | The Hague University of Applied Sciences | 2014 - 2019    |
| Mechanical Engineering | The Hague University of Applied Sciences | 2012 - 2013    |


###**Talks**

#### 9th Machine Learning NL Meetup hosted by KPN ICT Consulting [link](https://www.meetup.com/nl-NL/Machine-Learning-Netherlands/events/256143474/)
At KPN ICT Consulting we're often faced with the challenge to scale the deployments of analytics Python code.
In this talk I will explain what Dask is, how we use it and, more importantly, how you can use it to scale your existing analytics Python code.

###**Skills**

| Languages                         | Operating Systems & sorts          |  Other Languages (fluent) |
|-----------------------------------|------------------------------------|------------------------------|
| Python, Bash, R, HCL (Terraform), Ansible, SQL, VBA, .NET, C | Windows, Linux (Ubuntu, CentOS), Hadoop+ | Dutch, English |


###**Personal projects (P) & Quick hacks (Q) & Study projects (S) & Open Source (O)**

#####2019:
* (S) Predicting the relative risk of failure of a node in a telecom network
* (O) Dask Dataframe d-type inference consolidation and rewrite (In Progress)

#####2018:
* (P) Terraform, Terraform-Inventory, Ansible adventures for on-demand clusters and environments (write-up upcoming)
* (O) OpenStreetMaps & Nominatim as a single docker container for offline use (write-up upcoming)
* (P) Race data video analysis pipeline (In progress) [link](https://needsmore.xyz/race_data_video_analysis_pipeline.html)
* (O) Timelord, a framework for forecasting probability time series of binary variables. [link](https://gitlab.com/timelord/timelord)

#####2017:

* (Q) A brief analysis of a Reddit AMA [link](https://needsmore.xyz/a_brief_analysis_of_a_reddit_ama.html)
* (P) True 3D sound in Python: [link](https://needsmore.xyz/true_3d_sound.html)
* (P) Tor & Polipo proxies for Scrapy: [link](https://needsmore.xyz/tor_polipo_proxies_for_scrapy.html)
* (Q) Toying around with a Genetic Algorithm: [link](https://needsmore.xyz/toy_example_genetic_algorithm.html)

#####2016:

* (P) Self-written crawler framework: [link](https://needsmore.xyz/self_written_crawler_attempt.html)
* (Q) Reading back-numbers of cyclists: [link](https://needsmore.xyz/reading_heads_and_back_numbers.html)
* (S) An initial TSP solution: [link](https://needsmore.xyz/initial_tsp_solution.html) (solo study project)
* (Q) A Youtube Channel downloader: [link](https://needsmore.xyz/quick_youtube_dl_fix.html)
* (Q) A simple WhatsApp chat analysis: [link](https://needsmore.xyz/simple_whatsapp_analysis.html)

#####2015:

* (P) Titanic & Whale recognition Kaggle challenges [link](https://needsmore.xyz/titanic_whale_recog_kaggle.html)
* (P) Automatic number plate recognition (ANPR) in Python [link](https://needsmore.xyz/ANPR_project_description.html)

###**Hobbies**

* Auto racing [Youtube Channel](https://www.youtube.com/channel/UChv-wMEWriBEIWkc679aN5Q)
* Snowboarding
* Fitness
* Reading wikipedia


Contact me on: САЅPЕR [at] needsmore.xyz