Title: Youtube Channel Backer
Date: 25-02-2017
Category: Quick Hacks
Slug: quick_youtube_dl_fix

* Word count: 564
* Average reading time: 2 minutes and 49 seconds (based on 200 WPM)

[Show me the code](https://gitlab.com/tsktsktsktsk/public_yt_dl_repo/tree/master)


#When 
Somewhere in the middle of 2016

#What
A very brief extension of youtube-dl that neatly downloads every (available) youtube video of a channel, coded in approx. 1 hour.

#Why
Youtube removes, for obvious reasons, a lot of videos. When a video is removed there is no way of finding out what the tile of a video was. I use youtube for several years already
and have build a significant library over time. However when scrolling back through, for example, my favorites to figure out what I liked to listen to back then I noticed that so many
titles were missing. I wanted to keep these titles, the audio I didn't care so much about but not knowing what the track's title was, was quite a let down. 

To prevent this I looked a bit online for possible ways to download playlists of youtube and quickly stumbled upon [youtube-dl](https://rg3.github.io/youtube-dl/). 
As the name, youtube-dl, suggests the purpose of this library was to rip youtube videos from a given URL, that url could be either a link to a video, playlist or a whole channel. However,
when pointed to the direction of a channel it would download this into a single folder, bundling all the playlists together. I wanted to have each playlist in their respective folder.


#How

Installing and setting youtube-dl up was a breeze, especially on Linux. However, YouTube uses multiple different audio formats: ```.opus, .ogg, .webm``` and some others.
Initially only wanting a list of titles I figured: If I already have the audio, than why not?

For the pupose of conversion to a more widely used ```.mp3``` ffmpeg was suggested in the docs. Support for this is already integrated in youtube-dl so all I had to do was install ffmpeg and 
hopefully it was able to find it automatically - no need for path editing - which was the case.

Having youtube-dl up and running I needed a command to download an entire channel. Such a command (for only audio) looks like this:

    youtube-dl --extract-audio --ignore-errors -o "COMPLETE_FILE_PATH/%(title)s.%(ext)s" "COMPLETE_URL"

having the command ready, all I needed to do was recognize the channels I wanted to download, build a folder for it and generate the respective commands.

From the initial URL I requested the HTML, from this I extract the relevant playlist URLS and names, and with this list I let python generate 2 things:

1. bash script with all the youtube-dl commands.
2. folder structure in the main path.

Making this a lovely 2 command operation in which I first call the Python script and shortly after the freshly generated Bash script. I tried running the generated bash script
from the python script to wrap it into a single command but the subprocess library threw some errors, which probably have something to do with a Python process calling a bash script
which in its turn calls another Python process. I didn't look much into why this is, and at the moment of writing this I still don't exactly know why that happens but my suspicion
goes to Python not allowing another Python interpreter to be ran inside a Python process, but then again, this is speculation and I'm not sure. It works as it is
and I didn't want to spend too much time into it as my goal had already been achieved.
