#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Casper'
SITENAME = u''
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Amsterdam'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
#FEED_ALL_ATOM = None
#CATEGORY_FEED_ATOM = None
#TRANSLATION_FEED_ATOM = None
#AUTHOR_FEED_ATOM = None
#AUTHOR_FEED_RSS = None

#GOOGLE_ANALYTICS = "UA-91946122-3"


# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)
         
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
    },
    'output_format': 'html5',
}

STATIC_PATHS = ['ANPR_images', 
                'test_files',
                'self_written_crawler_images',
                'whatsapp_chat_analysis_images',
                '3d_sound_files',
                'whale_files',
                'GA_files',
                'tsp_images',
                'profile_pic',
                'reddit_acc_analyses_images',
                'extra',
                'extra/favicon.ico',
                'backnumber_recognition_images',]

EXTRA_PATH_METADATA = {'extra/favicon.ico': {'path': 'favicon.ico'},}


PLUGIN_PATH = ['plugins', '/plugins', '../plugins']
PLUGINS = ['plugins.render_math', 'render_math',]

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
